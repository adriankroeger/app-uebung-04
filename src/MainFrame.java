import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Klasse erstellt Fenster, fuegt Komponenten hinzu<br>
 * wobei das Eingabefenster die Anzahl an Zeitabschnitten
 * darstellt<br>
 * welche durch den Startknopf nebenlaeufig berechnet werden
 */
public class MainFrame extends JFrame {

    /** Uebergebenes Bild in Pattern-Darstellung umgerechnet */
    private BufferedImage img;

    /**
     * Konstruktor erstellt alle Fensterkomponenten und fuegt
     * Funktionalitaeten der Knoepfe hinzu
     * @param filePath Dateipfad der Bilddatei
     */
    public MainFrame(String filePath){
        //Knoepfe werden erstellt
        JButton start = new JButton("Start");
        JButton exit = new JButton("Exit");
        JTextField input = new JTextField();
        JLabel rounds = new JLabel("Rounds");

        //Eingabepanels werden erstellt
        JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        topPanel.add(rounds);
        input.setPreferredSize(new Dimension(80, 20));
        topPanel.add(input);
        topPanel.add(start);

        JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bottomPanel.add(exit);


        //Fenstereinstellungen werden gesetzt
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("APP Übung 04");
        this.setResizable(false);
        this.setLayout(new BorderLayout());

        this.add(topPanel, BorderLayout.NORTH);
        this.add(bottomPanel, BorderLayout.SOUTH);

        try{
            //Bild wird eingelesen, in Pattern umgerechnet
            img = toPattern(ImageIO.read(new File(filePath)));
            //und in einen zellulaeren Automaten geladen
            CellularAutomaton ca = new CellularAutomaton(img);

            AutomatonPanel imgPanel = new AutomatonPanel(ca);

            //Malflaeche soll ganzen Automaten zeichnen koennen
            imgPanel.setPreferredSize(new Dimension(ca.getSize(), ca.getSize()));
            this.add(imgPanel);

            //Thread wird mit noetigen Komponenten initialisiert und gestartet
            WorkerThread wt = new WorkerThread(ca, imgPanel);
            wt.start();

            start.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    synchronized(wt){
                        //Ueberpruefung ob Eingabe nicht-negative Zahl ist
                        if(input.getText().matches("[0-9]+")){
                            //Anzahl an Berechnungen wird gesetzt
                            wt.setIterations(Integer.parseInt(input.getText()));
                            //Thread wird benachrichtigt zu rechnen
                            wt.notify();
                        }
                        else{
                            System.err.println("Eingabe ist keine gültige Zahl");
                        }
                    }


                }
            });

            exit.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    System.exit(0);
                }
            });

            this.pack();
        }
        catch(IOException ioe){
            System.err.println("Bilddatei konnte nicht gelesen werden!");
            System.exit(1);
        }
        catch(NullPointerException npe){
            System.out.println("Datei ist im falschen Format oder nicht vorhanden!");
            System.exit(1);
        }


    }

    /**
     * Bekommt ein Bild uebergeben und rechnet die Pixel in Pattern um
     * @param original Bild welches in Pattern umgerechnet werden soll
     * @return Patterndarstellung des uebergebenen Bildes
     */
    private BufferedImage toPattern(BufferedImage original){
        BufferedImage pattern = new BufferedImage(original.getWidth()*2,
                                                  original.getHeight()*2,
                                                  BufferedImage.TYPE_INT_RGB);

        //top/bottom-left/right
        int tl = 0, tr = 0, bl = 0, br = 0;
        final int white = Color.WHITE.getRGB();
        final int black = Color.BLACK.getRGB();

        for(int x = 0; x < original.getWidth(); x++){
            for(int y = 0; y < original.getHeight(); y++){
                //Auslesen der RGB-Werte
                int red = new Color(original.getRGB(x, y)).getRed();
                int green = new Color(original.getRGB(x, y)).getGreen();
                int blue = new Color(original.getRGB(x, y)).getBlue();

                //Graustufen Berechnung
                int gray = (red+green+blue)/3;



                switch(gray/52){
                    case 4: {
                        tl = white;
                        tr = white;
                        bl = white;
                        br = white;
                    } break;
                    case 3: {
                        tl = black;
                        tr = white;
                        bl = white;
                        br = white;
                    } break;
                    case 2: {
                        tl = black;
                        tr = white;
                        bl = white;
                        br = black;
                    } break;
                    case 1: {
                        tl = white;
                        tr = black;
                        bl = black;
                        br = black;
                    } break;
                    case 0: {
                        tl = black;
                        tr = black;
                        bl = black;
                        br = black;
                    } break;
                    default: break;
                }

                //Pixel werden nach Pattern gesetzt
                pattern.setRGB(x*2, y*2, tl);
                pattern.setRGB(x*2+1, y*2, tr);
                pattern.setRGB(x*2, y*2+1, bl);
                pattern.setRGB(x*2+1, y*2+1, br);

            }
        }

        return pattern;
    }

    /**
     * Zeichnet eine grafische Darstellung des uebergebenen Automatens
     * auf die Malflaeche
     */
    class AutomatonPanel extends JPanel {

        /** Automat, dessen Zellen gezwichnet werden sollen*/
        private CellularAutomaton ca;

        /**
         * Konstruktor setzt den Automaten des Panels
         * @param ca Automat, dessen Zellne gezeichnet werdne sollen
         */
        private AutomatonPanel(CellularAutomaton ca){
            this.ca = ca;
        }

        /**
         * Zeichnet die Zellen des Automaten <br>
         *     (tot = weiss, lebendig = schwarz)<br>
         * auf die Malflaeche
         * @param g Graphics-Objekt des Panels
         */
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            g.setColor(Color.BLACK);

            //Zellen werden durchiteriert
            boolean[][] cells = ca.getCells();

            for(int x = 0; x < ca.getSize(); x++){
                for(int y = 0; y < ca.getSize(); y++){
                    //nur lebendige Zellen werden gezeichnet
                    if(cells[x][y]){
                        g.fillRect(x, y, 1, 1);
                    }
                }
            }
        }
    }

    /**
     * Thread berechnet zu einem uebergebenen Zellulaeren Automaten
     * eine vorgegebene Anzahl an Zeitabschnitten und laesst in jedem
     * Durchlauf die uebergebene Malflaeche zeichnen
     */
    class WorkerThread extends Thread {

        /** Zellulaerer Automat dessen Zustaende berechnet werden */
        private CellularAutomaton ca;
        /** JPanel auf das gezeichnet werden soll*/
        private AutomatonPanel ap;

        /** Anzahl der Berechnungen neuer Zustaende */
        private int iterations = 0;

        /** Indikator dass der Thread stoppen soll*/
        private boolean shouldStop = false;

        /**
         * Konstruktor setzt Automaten, Panel und monitor
         * @param ca Zellulaerer Automat dessen Zustaende berechnet werden
         * @param ap JPanel auf das gezeichnet werden soll
         */
        private WorkerThread(CellularAutomaton ca, AutomatonPanel ap){
            this.ca = ca;
            this.ap = ap;
        }

        /**
         * Setzt die Anzahl an Zeitabschnitten die der Thread berechnet
         * @param iterations anzahl zu berechnender zeitabschnitte
         */
        private void setIterations(int iterations){
            this.iterations = iterations;
        }

        /**
         * Wartet darauf dass Thread benachrichtigt wird und berechnet
         * vorgegebene Anzahl an Zeitabschnitten.<br>
         * Nach jeder Berechnung wird die grafische Darstellung aktualisiert
         */
        @Override
        synchronized public void run(){

            try{
                while(!shouldStop){
                    //wartet auf Knopfdruck
                    this.wait();
                    for(int i = 0; i < iterations; i++){
                        ca.nextState();
                        ap.repaint();
                    }
                }
            }
            catch(InterruptedException ie){
                System.err.println("Es ist ein Fehler beim Prozess aufgetreten");
            }
        }

    }
}


