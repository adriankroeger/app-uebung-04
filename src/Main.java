import java.io.File;

/**
 * Hauptklasse liest Dateipfad ein, kontrolliert ob
 * Datei existiert und legt Fenster an.
 */
public class Main {

    /**
     * Hauptmethode ueberprueft Eingabe und erstellt Fenster
     * @param args Dateipfad
     */
    public static void main(String[] args) {
        if(args.length == 0){
            System.err.println("Keinen Dateipfad angegeben!");
            return;
        }

        String path = args[0];

        //Pruefen ob Datei existiert
        File f = new File(path);
        if(!f.exists()){
            System.err.println("Datei existiert nicht!");
            return;
        }

        //Fenster wird mit Dateipfad erstellt
        MainFrame frame = new MainFrame(path);
        frame.setVisible(true);
    }
}
