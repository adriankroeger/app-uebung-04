import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Klasse realisiert einen Zellulaeren Automaten.<br>
 * Speichert aktuellen und naechsten Zustand.
 */
public class CellularAutomaton {

    /** Zellen im aktuellen Zustand */
    private boolean[][] cells;
    /** Zellen im naechsten zustand */
    private boolean[][] next;

    /**
     * Konstruktor initialisiert tote Zellen und
     * laedt uebergebenes Bild in den Automaten, wobei
     * schwarz = lebendig, weiss = tot
     * @param img Bild welches in den Automaten geladen wird
     */
    public CellularAutomaton(BufferedImage img){
        //Groesse wird so gesetzt, dass das Bild mindestens 6 mal
        //in der Hoehe und Breite passt
        int max = Math.max(img.getHeight(), img.getWidth());
        int size = (int)Math.pow(2, Math.ceil(Math.log(6*max)/Math.log(2)));
        //Automat wird mit toten Zellen initialisiert
        cells = new boolean[size][size];

        //Position (oben links) an die das Bild eingelesen wird
        //entspricht Mitte des Automaten
        int x = (size-img.getWidth())/2;
        int y = (size-img.getHeight())/2;

        //Zellen fuer das Bild werden gesetzt
        for(int i = 0; i < img.getWidth(); i++){
            for(int j = 0; j < img.getHeight(); j++){
                cells[x+i][y+j] = img.getRGB(i, j) == Color.BLACK.getRGB();
            }
        }
        //erster naechster zustand wird berechnet
        nextState();
    }

    /**
     * Gibt die Anzahl an Zellen pro Reihe bzw. Spalte zurueck
     * @return Kantenlaenge des Automaten
     */
    public int getSize(){
        return cells.length;
    }

    /**
     * Gibt die Inhalte der Zellen im Automaten zurueck
     * @return Array der Zellen
     */
    public boolean[][] getCells(){
        return cells;
    }

    /**
     * Berechnet zum aktuellen Zustand den Folgenden.<br>
     *
     */
    public void nextState(){
        if(next != null){
            cells = next;
        }
        //next wird neue boolean[][] adresse zugeteilt
        next = new boolean[getSize()][getSize()];

        for(int x = 0; x < getSize(); x++){
            for(int y = 0; y < getSize(); y++){
                //Nachbarwerte werden bestimmt
                boolean[] neighbours = new boolean[8];
                neighbours[0] = cells[Math.floorMod(x-1, getSize())][Math.floorMod(y-1, getSize())];
                neighbours[1] = cells[Math.floorMod(x, getSize())][Math.floorMod(y-1, getSize())];
                neighbours[2] = cells[Math.floorMod(x+1, getSize())][Math.floorMod(y-1, getSize())];
                neighbours[3] = cells[Math.floorMod(x-1, getSize())][Math.floorMod(y, getSize())];
                neighbours[4] = cells[Math.floorMod(x+1, getSize())][Math.floorMod(y, getSize())];
                neighbours[5] = cells[Math.floorMod(x-1, getSize())][Math.floorMod(y+1, getSize())];
                neighbours[6] = cells[Math.floorMod(x, getSize())][Math.floorMod(y+1, getSize())];
                neighbours[7] = cells[Math.floorMod(x+1, getSize())][Math.floorMod(y+1, getSize())];

                int i = 0;
                //anzahl an lebendigen Zellen wird bestimmt
                for(boolean b: neighbours){
                    if(b){
                        i++;
                    }
                }
                //ist anzahl ungerade ist zelle im folgenden zustand lebendig
                next[x][y] = (i%2 != 0);
            }
        }
    }

}
